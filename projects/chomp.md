---
title: 'Chomp'
emoji: 🍪
startDate: '2018-09-01'
endDate: 
summary: 'Food search engine with a robust API.'
---

### Highlights
- Created an algorithm that detects undesired ingredients in foods.
- Developed and maintain a database with detailed data for over 900,000 foods from around the world.
- Built a food [search engine](https://chompthis.com) and mobile [apps](https://chompthis.com/app) that have more than 20,000 monthly users.
- Created an [API](https://chompthis.com/api) and license my food data to a number of companies. In 2020, the API was voted the [#3 Product of the Day](https://www.producthunt.com/posts/chomp-food-nutrition-database) on Product Hunt and has since been recognized as one of the top APIs for food data.
- Configured and manage Ubuntu server on DigitalOcean.