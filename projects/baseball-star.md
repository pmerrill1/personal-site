---
title: 'Baseball Star'
emoji: ⭐
startDate: '2017-11-01'
endDate: 
summary: 'Realistic text-based online baseball career simulator game.'
---

### Highlights
- Realistic text-based online baseball career simulator [game](https://baseball-star.com).
- Users manage virtual baseball player careers from start to finish, with realistic progression based on in-game performance.
- Based on a board game that I developed (and produced).
- Simulation engine written in PHP.
- Every possible at bat outcome mapped to the role of 2-4 die, then translated into code.
- Over 1 million baseball games simulated with detailed data stored in a MySQL database.