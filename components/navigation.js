import utilStyles from '../styles/utils.module.css'
import Link from 'next/link'

function Navigation() {
    return (
        <div className={utilStyles.navBar}>
            <Link href={process.env.domain}>
                <a className={`${utilStyles.headingMd} ${utilStyles.navLink}`}>🙋🏻‍♂️ About Me</a>
            </Link>
            <Link href={`${process.env.domain}/projects`}>
                <a className={`${utilStyles.headingMd} ${utilStyles.navLink}`}>👨🏻‍💻 Projects</a>
            </Link>
            <Link href={`${process.env.domain}/work`}>
                <a className={`${utilStyles.headingMd} ${utilStyles.navLink}`}>👷🏻‍♂️ Work</a>
            </Link>
        </div>
    )
}

export default Navigation