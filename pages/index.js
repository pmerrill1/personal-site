import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import utilStyles from '../styles/utils.module.css'

export default function Home() {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>👋🏻 Hello! I'm a Software Engineer at <a href="https://www.rxante.com" target="_blank">RxAnte</a>. Our software helps healthcare providers manage medications for millions of vulnerable Medicare and Medicaid patients with complex histories.</p>
        <p>I have been a Software Engineer since 2011 and have been involved in a number of Brownfield and Greenfield projects. Most of my recent work has been with PHP (Laravel, Drupal, and procedural) and JS frameworks such as Next.js, Ionic, and React Native.</p>
        <p>Feel free to contact me on <a href="http://linkedin.com/in/petermerrill" target="_blank">LinkedIn</a> if you'd like to chat.</p>
      </section>
    </Layout>
  )
}