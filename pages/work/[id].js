import utilStyles from '../../styles/utils.module.css'
import Date from '../../components/date'
import Layout from '../../components/layout'
import Head from 'next/head'
import { getAllWorkIds, getWorkData } from '../../lib/work'

export async function getStaticProps({ params }) {
  const workData = await getWorkData(params.id)
  return {
    props: {
      workData
    }
  }
}

export async function getStaticPaths() {
    const paths = getAllWorkIds()
    return {
      paths,
      fallback: false
    }
  }

export default function Work({ workData }) {
  return (
    <Layout>
      <Head>
        <title>{workData.title}</title>
      </Head>
      <article>
        <h1 className={utilStyles.headingXl}>{workData.emoji} {workData.title}</h1>
        <span className={utilStyles.subtitleXl}>{workData.position}</span>
        <div className={`${utilStyles.dateXl} ${utilStyles.lightText}`}>
            <Date dateString={workData.startDate} /> - {workData.endDate ? <Date dateString={workData.endDate} /> : 'Present'}
        </div>
        <div dangerouslySetInnerHTML={{ __html: workData.contentHtml }} />
      </article>
    </Layout>
  )
}