import Link from 'next/link'
import Date from '../../components/date'
import Head from 'next/head'
import Layout, { siteTitle } from '../../components/layout'
import utilStyles from '../../styles/utils.module.css'

import { getSortedWorkData } from '../../lib/work'

export async function getStaticProps() {
  const allWorkData = getSortedWorkData()
  return {
    props: {
      allWorkData
    }
  }
}

export default function Work({ allWorkData }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Work</h2>
        <ul className={utilStyles.list}>
          {allWorkData.map(({ id, title, emoji, position, startDate, endDate, summary }) => (
            <li className={utilStyles.listItem} key={id}>
              <Link href={`/work/${id}`}>
                <a className={utilStyles.title}>{emoji} {title}</a>
              </Link>
              <br />
              <span className={utilStyles.subtitle}>{position}</span>
              <small className={`${utilStyles.date} ${utilStyles.lightText}`}>
                <Date dateString={startDate} /> - {endDate ? <Date dateString={endDate} /> : 'Present'}
              </small>
              <br/>
              <span className={utilStyles.summary}>{summary}</span>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}