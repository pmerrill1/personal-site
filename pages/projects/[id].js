import utilStyles from '../../styles/utils.module.css'
import Date from '../../components/date'
import Layout from '../../components/layout'
import Head from 'next/head'
import { getAllProjectIds, getProjectData } from '../../lib/projects'

export async function getStaticProps({ params }) {
  const projectData = await getProjectData(params.id)
  return {
    props: {
      projectData
    }
  }
}

export async function getStaticPaths() {
    const paths = getAllProjectIds()
    return {
      paths,
      fallback: false
    }
  }

export default function Project({ projectData }) {
  return (
    <Layout>
      <Head>
        <title>{projectData.title}</title>
      </Head>
      <article>
      <h1 className={utilStyles.headingXl}>{projectData.emoji} {projectData.title}</h1>
        <span className={utilStyles.subtitleXl}>{projectData.position}</span>
        <div className={`${utilStyles.dateXl} ${utilStyles.lightText}`}>
            <Date dateString={projectData.startDate} /> - {projectData.endDate ? <Date dateString={projectData.endDate} /> : 'Present'}
        </div>
        <div dangerouslySetInnerHTML={{ __html: projectData.contentHtml }} />
      </article>
    </Layout>
  )
}