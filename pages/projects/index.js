import Link from 'next/link'
import Date from '../../components/date'
import Head from 'next/head'
import Layout, { siteTitle } from '../../components/layout'
import utilStyles from '../../styles/utils.module.css'

import { getSortedProjectsData } from '../../lib/projects'

export async function getStaticProps() {
  const allProjectsData = getSortedProjectsData()
  return {
    props: {
      allProjectsData
    }
  }
}

export default function Projects({ allProjectsData }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Projects</h2>
        <ul className={utilStyles.list}>
          {allProjectsData.map(({ id, title, emoji, summary, startDate, endDate }) => (
            <li className={utilStyles.listItem} key={id}>
              <Link href={`/projects/${id}`}>
                <a className={utilStyles.title}>{emoji} {title}</a>
              </Link>
              <br />
              <small className={`${utilStyles.date} ${utilStyles.lightText}`}>
                <Date dateString={startDate} /> - {endDate ? <Date dateString={endDate} /> : 'Present'}
              </small>
              <br />
              <span className={utilStyles.summary}>{summary}</span>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}