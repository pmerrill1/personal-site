---
title: 'RotoWire'
emoji: 🏈
position: 'Senior Software Engineer'
startDate: '2015-02-01'
endDate: '2021-05-01'
summary: 'Industry leading sports news and fantasy sports tools.'
---

### Highlights
- Lead developer of [RotoWire's](https://rotowire.com) daily fantasy sports platform.
- Managed large SQL databases (SSMS & T-SQL) with business-critical data.
- Point of contact for stakeholders and customer support.
- Time-sensitive work that occasionally required urgent fixes outside business hours.
- API integrations and support.
- Refactored ColdFusion, Classic ASP, and VBScript legacy code into modern languages.

### Details

RotoWire is an industry leader in sports news, fantasy sports content, and daily fantasy sports tools. As a Senior Software Engineer, I oversaw all projects related to RotoWire's daily fantasy sports tools, which were used by thousands of concurrent customers.