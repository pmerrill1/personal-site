---
title: 'RxAnte'
emoji: 🏥
position: 'Software Engineer'
startDate: '2021-06-01'
endDate:
summary: 'Innovative software solutions for healthcare providers.'
---

### Highlights
- Build, release, and maintain both of [Rxante's](https://www.rxante.com) innovative healthcare software products.
- Wire up UI on the frontend, code backend business logic, and tackle issues throughout the codebase.
- Worked as part of a team to integrate Salesforce into pharmacy operations.
- Working remotely.

### Details

Rxante delivers analytics-powered solutions and clinical services that enable health plans, care providers, and pharmaceutical manufacturers to realize more from medicines.

We have millions of lives under management, and our patented solutions for health plans are proven to improve quality scores and lower costs.

Additionally, RxAnte's Mosaic Pharmacy Service is designed specifically to provide high-value pharmacy services to the most vulnerable members of Medicare Advantage and Part D plans.