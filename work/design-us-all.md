---
title: 'Design Us All'
emoji: 👨🏻‍💻
position: 'Freelance Software Engineer'
startDate: '2011-12-01'
endDate: '2014-05-01'
summary: 'Freelance contractor doing work for a number of clients.'
---

### Highlights
- Freelance contractor doing business as Design Us All.
- Created a Pinterest-style website, a social network, a peer-to-peer lending platform, and multiple WordPress websites.
- Developed multiple iOS apps.
- Hosted and managed client websites.
- Designed marketing materials.