---
title: 'Fanium'
emoji: 📱
position: 'Software Engineer (Web/Android)'
startDate: '2013-02-01'
endDate: '2015-01-01'
summary: 'The first exclusively mobile fantasy sports games.'
---

### Highlights
- Developed several fantasy sports apps.
- Front-end designer and developer.
- Worked remotely.
- Designed email marketing campaigns.
- Company was bought by CBS.