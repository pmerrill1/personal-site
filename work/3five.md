---
title: '3five Designs'
emoji: 🧑🏻‍🎓
position: 'Junior Designer, Front-end Developer, and Mobile App Developer'
startDate: '2011-09-01'
endDate: '2011-12-01'
summary: 'First professional web dev work.'
---

### Highlights
- Works on a number of client WordPress websites.
- Redesigned client support website.
- Developed iOS app.
- Left to take on my own freelance work.